#include <sys/time.h>

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/*
	c = a + i*b;
	z = x + i*y;
	while(abs(z) < inf)
		z = z*z + c;
*/

static void usage(void) {
	fprintf(stderr,
		"usage: mandelbrot [-f] [-c a,b] [-s w,h] [-z p] <outfile>\n"
		"	-f	force overwrite output\n"
		"	-c a,b	centre image on c = a + i*b\n"
		"	-s w,h	size of image is w*h pixels\n"
		"	-z p	width of image is 2^-p\n"
		);
	exit(1);
}

static void check(char *p, char *q, int c) {
	if(errno != 0)
		err(1, "parse error");
	if(p == q)
		errx(1, "junk at start of argument");
	if(*q != c)
		errx(1, "junk at end of argument");
}

int main(int argc, char *argv[]) {

	int openmode = O_WRONLY|O_CREAT|O_EXCL;

	/* size of image */
	long ipx = 720;
	long rpx = 1280;

	/* centre of image */
	double cb = +1.03969513783305;
	double ca = -0.15284673572977;

	/* log range of image */
	double zoom = -2.0;

	int opt;
	char *p, *q;
	while((opt = getopt(argc, argv, "fc:s:z:")) != -1)
		switch (opt) {
		case('f'): /* force overwrite */
			openmode = O_WRONLY|O_CREAT|O_TRUNC;
			break;
		case('c'): /* centre */
			errno = 0;
			ca = strtod(optarg, &p);
			check(optarg, p, ',');
			cb = strtod(p+1, &q);
			check(p+1, q, '\0');
			break;
		case('s'): /* size */
			errno = 0;
			rpx = strtol(optarg, &p, 10);
			check(optarg, p, ',');
			ipx = strtol(p+1, &q, 10);
			check(p+1, q, '\0');
			break;
		case('z'): /* zoom */
			errno = 0;
			zoom = strtod(optarg, &p);
			check(optarg, p, '\0');
			break;
		default:
			usage();
		}
	argc -= optind;
	argv += optind;
	if(argc != 1)
		usage();
	int ofd = open(*argv, openmode, 0666);
	if(ofd < 0)
		err(1, "open %s", *argv);

	int colourful = rpx * ipx / 4;
	int quality = (rpx < ipx ? rpx : ipx) / 10;

	/* range of image and of pixel */
	double asz = pow(2.0, -zoom);
	double psz = asz / rpx;
	double bsz = psz * ipx;

	/* origin of image */
	double ob = cb + bsz / 2.0;
	double oa = ca - asz / 2.0;

	/* number of pixels */
	long npx = rpx * ipx;

	/* We adaptively adjust the maximum number of iterations to
	ensure we get a good image. We maintain a queue of pixels that
	might be members of the set, with the value of z for each
	pixel when we last gave up iterating it. In the main loop we
	run through the queue iterating each pixel several times to
	see if it escapes. If no pixels escape during a run through
	the queue then we declare the image complete. */

	struct px {
		long apx, bpx;
		double x, y;
	};

	/* set up in and out queues */

	struct px *iq = calloc(npx, sizeof(struct px));
	struct px *oq = calloc(npx, sizeof(struct px));

	/* ensure queue is in raster order */

	struct px *ie = iq;
	for(long bpx = 0; bpx < ipx; bpx++) {
		for(long apx = 0; apx < rpx; apx++) {
			ie->bpx = bpx;
			ie->apx = apx;
			ie++;
		}
	}

	/* final image */
	double *esc = calloc(npx, sizeof(double));
	int coloured = 0;

	/* performance */
	double work = 0;
	struct timeval stv;
	gettimeofday(&stv, NULL);

	double ln2 = log(2.0);

	/* range of iterations in this run through the queue */
	int miniter = 0;
	int maxiter = 1000;
	for(;;) {
		struct px *op = oq;
		for(struct px *ip = iq; ip < ie; ip++) {
			double b = ob - psz * ip->bpx;
			double a = oa + psz * ip->apx;
			double y = ip->y;
			double x = ip->x;
			double y2 = y * y;
			double x2 = x * x;

			int i;
			for(i = miniter; i < maxiter; i++) {
				y = 2.0 * x * y + b;
				x = x2 - y2 + a;
				y2 = y * y;
				x2 = x * x;
				/* http://fanf.livejournal.com/110025.html */
				double r2 = x2 + y2;
				if(r2 > 65536) {
					long px = ip->bpx * rpx + ip->apx;
					esc[px] = i - log(log(r2)) / ln2;
					coloured++;
					goto cont;
				}
			}
			/* pixel might still be in set so save for next run */
			op->bpx = ip->bpx;
			op->apx = ip->apx;
			op->y = y;
			op->x = x;
			op++;
		cont:
			work += i - miniter;
		}
		/* how many pixels removed from queue */
		size_t ni = ie - iq;
		size_t no = op - oq;
		int delta = ni - no;

		warnx("%f iterating to %d removed %d pixels",
		      zoom, maxiter, delta);

		/* Check the image is sufficiently colourful and of
		sufficiently high quality. This can lead to an infinite
		loop if there is too much Mandelbrot set in the frame. */
		if(coloured > colourful && delta < quality)
			break;
		/* Avoid stalling just above quality threshold. */
		if(delta < quality * 2)
			quality *= 2;

		/* swap queues */
		struct px *tmp = iq;
		iq = oq;
		ie = op;
		oq = tmp;

		/* next chunk of iterations */
		miniter = maxiter;
		maxiter += maxiter/2;
	}

	struct timeval etv;
	gettimeofday(&etv, NULL);

	double st = stv.tv_sec + stv.tv_usec / 1000000.0;
	double et = etv.tv_sec + etv.tv_usec / 1000000.0;
	double tt = et - st;

	warnx("%f total iterations %f", zoom, work);
	warnx("%f total time %f", zoom, tt);
	warnx("%f rate %g iterations/second", zoom, work/tt);

	int nb = npx * sizeof(double);
	int r = write(ofd, esc, nb);
	if(r != nb || close(ofd) < 0) {
		unlink(*argv);
		err(1, "write %s", *argv);
	}

	return(0);
}
