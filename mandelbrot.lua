-- parameters

local hpx, vpx = 320, 320

local ca, cb = -0.15284673572977, 1.03969513783305

local a_size = 4

if arg[1] then a_size = 2^(2-arg[1]) end
if arg[2] and arg[3] then ca, cb = arg[2], arg[3] end
if arg[4] and arg[5] then hpx, vpx = arg[4], arg[5] end

local mul = a_size / hpx
local b_size = vpx * mul

local oa = ca - a_size/2
local ob = cb + b_size/2

local capx = hpx/2
local cbpx = vpx/2

local maxiter = 100000
local maxdiam = 1000

-- external depencencies

local log = math.log
local log2 = log(2)

local stderr = io.stderr
local write = io.write
local chars = string.char

-- one pixel

local function potential(a,b)
   local i = 0
   local x, y = 0, 0
   local x2, y2 = 0, 0
   local function step()
      i = i + 1
      y = 2 * x * y + b
      x = x2 - y2 + a
      y2 = y * y
      x2 = x * x
   end
   while i < maxiter and x2 + y2 < maxdiam do
      step()
   end
   if i < maxiter then
      -- http://linas.org/art-gallery/escape/escape.html
      step()
      step()
      return log(i - log(log(x2 + y2) / 2) / log2)
   else
      return 0
   end
end

local function int(f)
   return f - f % 1
end

local colours = { { r =   0, g =   0, b =   0 },
		  { r =   0, g =   0, b = 255 },
		  { r =   0, g = 255, b =   0 },
		  { r = 255, g = 255, b = 255 },
		  { r = 255, g = 255, b =   0 },
		  { r = 255, g =   0, b =   0 },
		  { r =   0, g =   0, b =   0 } }
local nrgb = #colours - 1
local R, r = {}, {}
local G, g = {}, {}
local B, b = {}, {}

for i = 1, nrgb do
   R[i-1] = colours[i].r
   r[i-1] = colours[i+1].r - R[i-1]
   G[i-1] = colours[i].g
   g[i-1] = colours[i+1].g - G[i-1]
   B[i-1] = colours[i].b
   b[i-1] = colours[i+1].b - B[i-1]
end

local function colour(p)
   p = p % nrgb
   local i = int(p)
   p = p - i
   return R[i] + r[i] * p
        , G[i] + g[i] * p
        , B[i] + b[i] * p
end

-- whole picture

write(("P6 %d %d 255\n"):format(hpx,vpx))

for bpx = 1, vpx do
   local b = ob - bpx * mul
   if bpx % 40 == 0 then
      stderr:write(("%d %.20f\n"):format(bpx,b))
   end
   for apx = 1, hpx do
      local a = oa + apx * mul
      if bpx == 1 and apx % 40 == 0 then
	 stderr:write(("%d %.20f\n"):format(apx,a))
      end
      if apx % 40 == 0 or bpx % 40 == 0
      then
	 write(chars(128,128,128))
      else
	 write(chars(colour(potential(a,b))))
      end
   end
end

return
