CFLAGS=	-pipe -O3 -std=c99 -pedantic -U__STRICT_ANSI__ \
	-Wall -Wextra -Wbad-function-cast -Wcast-align \
	-Wcast-qual -Wchar-subscripts -Winline -Wmissing-prototypes \
	-Wnested-externs -Wpointer-arith -Wredundant-decls -Wshadow \
	-Wstrict-prototypes -Wwrite-strings

#G=  /home/fanf2/gcc
#CC= LD_LIBRARY_PATH=${G}/lib ${G}/bin/gcc -march=native -mtune=native

BIBTEX=	bibtex
LATEX= latex -halt-on-error
PDFLATEX= ${LATEX} -output-format=pdf
DVILATEX= ${LATEX} -output-format=dvi


all: mandelbrot mandel2ppm slides.pdf notes.pdf distance.pdf

clean:
	rm -f mandelbrot mandel2ppm
	rm -f icon.* eg[-.]*
	rm -f *.aux *.log *.nav *.out *.snm *.toc
	rm -f *.pdf *.ps *.dvi

icon.ppm: mandel2ppm icon.mandel
	./mandel2ppm -f -s 512,480 -c m icon.mandel icon.ppm

icon.mandel: mandelbrot
	./mandelbrot -f -s 512,480 -c -0.75,0 -z -1.5 icon.mandel

eg-fast.ppm: mandel2ppm eg.mandel
	./mandel2ppm -f -s 800,656 -c wf eg.mandel eg-fast.ppm

eg-step.ppm: mandel2ppm eg.mandel
	./mandel2ppm -f -s 800,656 -c ws eg.mandel eg-step.ppm

eg.mandel: mandelbrot Makefile
	./mandelbrot -f -s 800,656 -c -0.6,0 eg.mandel

eg-fade.ppm: mandel2ppm eg-fade.mandel
	./mandel2ppm -f -s 800,656 -c w eg-fade.mandel eg-fade.ppm

eg-fade.mandel: mandelbrot Makefile
	./mandelbrot -f -s 800,656 -c -0.6,0 -z -1.75 eg-fade.mandel

pretty.png: pretty.ppm
	convert -resize 800x600 pretty.ppm pretty.png

pretty.ppm: mandel2ppm pretty.mandel
	./mandel2ppm -f -s 1600,1200 pretty.mandel pretty.ppm

pretty.mandel: mandelbrot
	./mandelbrot -f -s 1600,1200 -c -0.7436433,0.131826 -z 16 pretty.mandel

IMAGES= icon.png pretty.png eg-fade.png eg-fast.png eg-step.png

${IMAGES}: ${IMAGES:.png=.ppm}

mandelbrot: mandelbrot.c

mandel2ppm: mandel2ppm.c

slides.ps slides.pdf slides.dvi: talk.tex slides.tex ${IMAGES}

notes.ps  notes.pdf  notes.dvi:  talk.tex talk.bib notes.tex

.SUFFIXES: .c .tex .dvi .pdf .ps .ppm .png

.c:
	${CC} ${CFLAGS} -o $@ $< -lm

.ppm.png:
	convert $< $@

.dvi.ps:
	dvips $<

.tex.pdf:
	${PDFLATEX} $<
	${BIBTEX} $*
	${PDFLATEX} $<
	${PDFLATEX} $<

.tex.dvi:
	${DVILATEX} $<
	${BIBTEX} $*
	${DVILATEX} $<
	${DVILATEX} $<
