#include <sys/time.h>

#include <assert.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static void usage(void) {
	fprintf(stderr,
		"usage: mandel2ppm [-f] [-c m] [-s w,h] <infile...> <outfile>\n"
		"	-f	force overwrite output\n"
		"	-s w,h	size of image is w*h pixels\n"
		"	-c m	colouring mode\n"
		"		b	fade from black\n"
		"		w	fade from white\n"
		"		wf	fast fade from white\n"
		"		ws	fast step from white\n"
		"		m	monochrome\n"
		"	multiple infiles are blurred together\n"
		);
	exit(1);
}

static void check(char *p, char *q, int c) {
	if(errno != 0)
		err(1, "parse error");
	if(p == q)
		errx(1, "junk at start of argument");
	if(*q != c)
		errx(1, "junk at end of argument");
}

typedef unsigned char byte;
struct rgb {
	byte r, g, b;
} black = { 0, 0, 0 },
  grey = { 127, 127, 127 },
  white = { 255, 255, 255 };

typedef struct rgb pixel_fun(double p);

static struct rgb black_fade(double p) {
	if(p == 0) return(black);
	if(p < 0) return(black);
	p = log(p);
	if(p < 0) return(black);
	struct rgb rgb;
	rgb.r = 255 * 0.5 * (1.0 - cos(p * 1.0));
	rgb.g = 255 * 0.5 * (1.0 - cos(p * 2.0));
	rgb.b = 255 * 0.5 * (1.0 - cos(p * 3.0));
	return(rgb);
}

static struct rgb white_fade(double p) {
	if(p == 0) return(black);
	if(p < 0) return(white);
	p = log(p);
	if(p < 0) return(white);
	struct rgb rgb;
	rgb.r = 255 * 0.5 * (1.0 + cos(p * 2.0));
	rgb.g = 255 * 0.5 * (1.0 + cos(p * 1.5));
	rgb.b = 255 * 0.5 * (1.0 + cos(p * 1.0));
	return(rgb);
}

static struct rgb white_fast(double p) {
	if(p == 0) return(black);
	if(p < 0) return(white);
	struct rgb rgb;
	rgb.r = 255 * 0.5 * (1.0 + cos(p * 2.0));
	rgb.g = 255 * 0.5 * (1.0 + cos(p * 1.5));
	rgb.b = 255 * 0.5 * (1.0 + cos(p * 1.0));
	return(rgb);
}

static struct rgb white_step(double p) {
	if(p == 0) return(black);
	if(p < 0) return(white);
	p = floor(p);
	if(p < 0) return(white);
	struct rgb rgb;
	rgb.r = 255 * 0.5 * (1.0 + cos(p * 2.0));
	rgb.g = 255 * 0.5 * (1.0 + cos(p * 1.5));
	rgb.b = 255 * 0.5 * (1.0 + cos(p * 1.0));
	return(rgb);
}

static struct rgb shade(double p) {
	if(p == 0) return(black);
	if(p < exp(1)) return(white);
	byte s = 255 / log(p);
	struct rgb rgb = { s, s, s };
	return(rgb);
}

int main(int argc, char *argv[]) {

	/* default options */

	int openmode = O_WRONLY|O_CREAT|O_EXCL;

	pixel_fun *pixellate = black_fade;

	/* size of image */
	long ipx = 720;
	long rpx = 1280;

	int x;
	while((x = getopt(argc, argv, "fc:s:")) != -1)
		switch (x) {
		case('f'): /* force overwrite */
			openmode = O_WRONLY|O_CREAT|O_TRUNC;
			break;
		case('c'): /* colour scheme */
			if(strcmp(optarg, "b") == 0)
				pixellate = black_fade;
			else
			if(strcmp(optarg, "w") == 0)
				pixellate = white_fade;
			else
			if(strcmp(optarg, "wf") == 0)
				pixellate = white_fast;
			else
			if(strcmp(optarg, "ws") == 0)
				pixellate = white_step;
			else
			if(strcmp(optarg, "m") == 0)
				pixellate = shade;
			else
				usage();
			break;
		case('s'): { /* size */
			char *p, *q;
			errno = 0;
			rpx = strtol(optarg, &p, 10);
			check(optarg, p, ',');
			ipx = strtol(p+1, &q, 10);
			check(p+1, q, '\0');
		} break;
		default:
			usage();
		}
	argc -= optind;
	argv += optind;
	if(argc < 2)
		usage();
	const char *ofn = argv[argc - 1];

	/* number of pixels */
	long npx = rpx * ipx;

	/* read inputs */
	int iby = npx * sizeof(double);
	int in = argc - 1;
	int fd[in];
	double *pot[in];
	for(int i = 0; i < in; i++) {
	  fd[i] = open(argv[i], O_RDONLY);
	  if(fd[i] < 0) err(1, "open %s", argv[i]);
	  pot[i] = malloc(iby);
	  if(pot[i] == NULL) err(1, "malloc");
	  x = read(fd[i], pot[i], iby);
	  if(x < 0) err(1, "read %s", argv[i]);
	  if(x < iby)
		  errx(1, "file %s too small", argv[i]);
	  if(read(fd[i], &x, 1) != 0)
		  errx(1, "file %s too large", argv[i]);
	  if(close(fd[i]) < 0)
		  errx(1, "read %s", argv[i]);
	}

	/* output image */
	assert(sizeof(struct rgb) == 3);
	int oby = npx * sizeof(struct rgb);
	struct rgb *pixmap = malloc(oby);
	if(pixmap == NULL) err(1, "malloc");
	int ofd = open(ofn, openmode, 0666);
	if(ofd < 0) err(1, "open %s", ofn);

	/* write ppm header */
	#define hsz 20
	char header[hsz];
	int hby = snprintf(header, hsz, "P6 %ld %ld 255\n", rpx, ipx);
	if(hby >= hsz) errx(1, "image size too large");
	if(write(ofd, header, hby) != hby) {
		unlink(ofn);
		err(1, "write %s", ofn);
	}

	/* convert */
	for(struct rgb *pix = pixmap; npx--; pix++) {
		double r = 0, g = 0, b = 0;
		for(int i = 0; i < in; i++) {
			struct rgb rgb = pixellate(*pot[i]++);
			double n = i + 1;
			r = r + (rgb.r - r) / n;
			g = g + (rgb.g - g) / n;
			b = b + (rgb.b - b) / n;
		}
		pix->r = r;
		pix->g = g;
		pix->b = b;
	}

	/* write output */
	if(write(ofd, pixmap, oby) != oby || close(ofd) < 0) {
		unlink(ofn);
		err(1, "write %s", ofn);
	}

	return(0);
}
